import mongoose from "mongoose";

const dengueDataSchema=mongoose.Schema({

    user:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
    },
    year:{
        type: Number,
        required: true,
    },
    month:{
        type: String,
        required: true,
    },
    districtId:{
        type: String,
        required: true,
    },
    dengueCases:{
        type: Number,
        required: true,
    },
    rainfall:{
        type: Number,
        required: true,
    },

},{

timestamps: true,

})

export default mongoose.model('dengueData', dengueDataSchema)