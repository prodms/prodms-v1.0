import asyncHandler from 'express-async-handler';
import dengueData from '../models/dngDataModel.js';
import User from '../models/userModel.js';
import generateToken from '../utils/generateToken.js'; 


//@desc     Get all dengue data
//@route    GET /api/dngData
// @access  Public
export const getDngData = asyncHandler(async (req, res) => {


  const dngData = await dengueData.find().sort({  year: -1, month: 1 });; // get the all dengue records
  const limit= parseInt(req.query.limit);

  if (!isNaN(limit) && limit >0){
      return res.status(200).json(dngData.slice(0, limit));
  } 
//    res.json({
//         _id: dngData._id,
//         user: dngData.user,
//         year: dngData.year,
//         month: dngData.month,
//         districtId: dngData.districtId,
//         dengueCases: dngData.dengueCases,
//         rainfall: dngData.rainfall,



//       });

res.status(201).json(dngData);
  
  
});

//@desc     Get a single dengue data
//@route    GET /api/dngData/:id
// @access  Public
export const getSingleDngData = asyncHandler(async (req,res)=>{
    
//     const id=parseInt(req.params.id)
// console.log(id)
    //const dngData = await dengueData.find({_id: id});
    const dngData = await dengueData.findById(req.params.id);
    res.status(200).json(dngData);

});


//@desc     Create a new dengue data
//@route    POST /api/dngData

export const createDngData = asyncHandler(async (req,res)=>{


    if (!req.body.dengueCases){
        res.status(400)
        throw new Error('Please insert number of dengue cases')
    }


    // Write the database -  collection name denguedata
    const dngData = await dengueData.create({
        user: req.user.id, // get from protect function in middleware... pls refer the routes file to find the link
        year: req.body.year,
        month: req.body.month,
        districtId: req.body.districtId,
        dengueCases: req.body.dengueCases,
        rainfall: req.body.rainfall,

        
    });
    res.status(201).json(dngData);

});

//@desc     Update dengue data
//@route    PUT /api/dngData/:id

export const updateDngData = asyncHandler(async (req,res)=>{


    const dngData = await dengueData.findById(req.params.id);

    if(!dngData){
        res.status(400);
        throw new Error('Dengue not found')
    }

// Check logged user authority for update the dengue cases...

const user= await User.findById(req.user.id)  // get logged user id

// Check login or not
if(!user){
    res.status(401)
    throw new Error('User not found')
}
console.log(user.userCat)
// Make sure the logged in user matches with admin profile
if(user.userCat !== (1||2)){
    res.status(401)
    throw new Error('User not authorized')
}

// const updatedDngData = await dengueData.findByIdAndUpdate(req.params.id, req.body, {
//     new: true,
// })
//res.status(200).json(updatedDngCases);

//////////////////////////////////////////////////////////////////////

    if (dngData) {
        dngData.user = req.user.id || dngData.user;
        dngData.year = req.body.year || dngData.year;
        dngData.month=req.body.month || dngData.month;
        dngData.districtId = req.body.districtId || dngData.districtId;
        dngData.dengueCases=req.body.dengueCases || dngData.dengueCases;
        dngData.rainfall = req.body.rainfall || dngData.rainfall;


  
      const updatedDengueData = await dngData.save();
  
      res.json({
        _id: updatedDengueData._id,
        user: updatedDengueData.user, 
        year: updatedDengueData.year,
        month: updatedDengueData.month,
        districtId: updatedDengueData.districtId,
        dengueCases: updatedDengueData.dengueCases,
        rainfall: updatedDengueData.rainfall,
      });
    } else {
      res.status(404);
      throw new Error('UsDengue data not found');
    }

});



//@desc     Delete dengue data
//@route    DELETE /api/dngData/:id



export const deleteDngData = asyncHandler(async (req,res)=>{
    const dngData = await dengueData.findById(req.params.id);

    console.log(req.params.id)
    if(!dngData){
        res.status(400);
        throw new Error('Dengue not found')
    }
// Check logged user authority for update the dengue cases...

const user= await User.findById(req.user.id)  // get logged user id

// Check login or not
if(!user){
    res.status(401)
    throw new Error('User not found')
}
console.log(user.userCat)
// Make sure the logged in user matches with admin profile
if(user.userCat !== (1||2)){
    res.status(401)
    throw new Error('User not authorized')
}

    await dengueData.deleteOne({ _id: req.params.id });

res.status(200).json({id: req.params.id});
});