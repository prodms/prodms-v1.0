import { fetchBaseQuery, createApi } from '@reduxjs/toolkit/query/react';

const baseQuery = fetchBaseQuery({ baseUrl: '' }); // we get proxy for get server url

export const apiSlice = createApi({
  baseQuery,
  tagTypes: ['DengueData', 'User'],
  endpoints: (builder) => ({}),
});
