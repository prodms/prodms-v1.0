// AdminDashboardScreen.jsx
import React from 'react';
import { Form,Container, Row, Col, Card } from 'react-bootstrap';
//import SideMenu from '../components/SideMenu';
import AdminSideMenu from '../components/AdminSideMenu';
import { useState, useEffect } from 'react';
import Heatmap from '../components/Heatmap';

import { useGetDngDataQuery} from '../slices/dngDataApiSlice';

import DengueCasesChart from '../components/DengueCasesChart';
import { districts } from '../config/config';

const AdminDashboardScreen = () => {

 // const [dengueData, setDengueData] = useState([]);
  const [selectedDistrict, setSelectedDistrict] = useState('LK-11');


  // Get Updated dengue case details


/////*********************   End first part */
    // Get dengue data
    const { data: dngData} = useGetDngDataQuery({});
  
    const handleDistrictChange = (event) => {
      setSelectedDistrict(event.target.value);
    };


  return (



    <Container fluid className="mt-3">
    <Row>
      <Col md={2}>
  
        <AdminSideMenu />
      </Col>
      <Col md={9}>
        <Card>
          <Card.Body>
          <div>
    <h1>Dengue Dynamics</h1>
    <Form.Group controlId="districtSelect">
        <Form.Label><h6>Select District</h6></Form.Label>
        <Form.Control as="select" value={selectedDistrict} onChange={handleDistrictChange}>
          {Object.entries(districts).map(([id, name]) => (
            <option key={id} value={id}>
              {name}
            </option>
          ))}
        </Form.Control>
      </Form.Group>

      <DengueCasesChart data={dngData} districtId={selectedDistrict} />
  </div>
          </Card.Body>
        </Card>
      </Col>
    </Row>

    <Row>
      <Col md={2}>
  
      </Col>
      <Col md={9}>
        <Card>
          <Card.Body>
          <div>
          <h4>Latest Dengue Hotspot</h4>

      <Heatmap />
    </div>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  </Container>
   
  );
};

export default AdminDashboardScreen;
